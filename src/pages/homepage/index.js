import React from 'react'
import Form from "../../component/Form"
import Form2 from "../../component/OponentName"
import User from "../../component/User"
import {UserStatsProvider, OponentStatsProvider} from '../../context'
import Oponent from "../../component/Oponent"


const Homepage = () => {
  return(
    <div>
    <Form />
    <UserStatsProvider>
    <User>
    </User>
    </UserStatsProvider>
    <div>
    <h1>VERSUS</h1>
    </div>
    <div>
    <Form2 />
    <OponentStatsProvider>
    <Oponent>
    </Oponent>
    </OponentStatsProvider>
    </div>
    </div>
  )
}

export default Homepage