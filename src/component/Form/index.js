import React from 'react'

class Form extends React.Component{
  constructor(props){
    super(props);
    this.state = {value:""};
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  handleChange(e){
    this.setState ({value:e.target.value});
  }
  handleSubmit(e){
    localStorage.setItem("nickname", this.state.value)
  }


  render(){
    return (
      <div>
      <form onSubmit={this.handleSubmit} >
      <input name="name" type="text" value={this.state.value} onChange={this.handleChange}/>
      <input name="Send" type="submit"/>

      </form>
      </div>
    )
  }
}


export default Form
