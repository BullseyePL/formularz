import {useContext} from 'react'
import {CharStats} from '../../context'

localStorage.setItem('oponentName', '')

const Oponent = (props) =>{
  const {name,lvl, str, hp, atkSpd} = useContext(CharStats)
  const checkOponentName = () => {
    if(localStorage.getItem('oponentName') === ""){
      return false
    }else return true
  }
  const showOponent = checkOponentName()
  return(
    <div>
    {showOponent ?
  <div>
    {props.children}
    <p>Name: {name}</p>
    <p>Level: {lvl}</p>
    <p>Strenght: {str}</p>
    <p>HitPoints: {hp}</p>
    <p>AttackSpeed: {atkSpd}</p>
  </div> :
    <div>Choose Your Name</div>
  }
  </div>
)
}
export default Oponent
