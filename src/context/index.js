import React, {createContext, useState} from 'react'


export const CharStats = createContext({
  name:"",
  lvl: 0,
  str: 0,
  hp: 0,
  atkSpd: 0,
})

export const UserStatsProvider = ({children}) => {
  const name = localStorage.getItem('nickname');
  const lvl = useState(Math.ceil(Math.random().toFixed(2)*30))
  const str = useState(Math.round(Math.random().toFixed(2)*50));
  const hp = useState(Math.round(Math.random().toFixed(2)*100));
  const atkSpd =useState(Math.round(Math.random().toFixed(2)*10))
  
  return (
    <CharStats.Provider value = {{
      name,
      lvl,
      str,
      hp,
      atkSpd,
    }}
    >
    {children}
    </CharStats.Provider>
  )
}

export const OponentStatsProvider = ({children}) =>{
  const name = localStorage.getItem('oponentName')
  const lvl = useState(Math.ceil(Math.random().toFixed(2)*10))
  const str = useState(Math.round(Math.random().toFixed(2)*10));
  const hp = useState(Math.round(Math.random().toFixed(2)*10));
  const atkSpd =useState(Math.round(Math.random().toFixed(2)*5))
  
  return (
    <CharStats.Provider value = {{
      name,
      lvl,
      str,
      hp,
      atkSpd
    }}
    >
    {children}
    </CharStats.Provider>
  )
}
