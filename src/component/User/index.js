import {useContext} from 'react'
import {CharStats} from '../../context'


localStorage.setItem('nickname', '')

const User = (props) => {
  const{name,lvl, str, hp, atkSpd,} = useContext(CharStats)
  const checkNickname = () =>{
    if(localStorage.getItem('nickname') === "" || localStorage.getItem('nickname' === null)){
      return false
    }else return true
  }

  const showStats = checkNickname()

  return(
    <div>
    {showStats ?
  <div>
    {props.children}
    <p>Name: {name}</p>
    <p>Level: {lvl}</p>
    <p>Strenght: {str}</p>
    <p>HitPoints: {hp}</p>
    <p>AttackSpeed: {atkSpd}</p>
  </div> :
    <div>Choose Your Name</div>
  }
  </div>

  )
}
export default User
